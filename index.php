<?php include_once("livewiki/config.php");
header("Cache-Control: private, no-transform");


if(file_exists("/var/www/html/vendor/atomjump/loop-server/config/adminAlreadySet.json")) {
	//Don't show an admin entering ability.
	$show_admin = false;
} else {
	//Do show an admin entering ability
	$show_admin = true;
}

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, user-scalable=no">
	
	 <!-- AtomJump Feedback Starts -->
	 <link rel="StyleSheet" href="livewiki/<?php echo $bootstrap_css_path ?>" rel="stylesheet">

	<!-- AtomJump Feedback CSS -->
	<link rel="StyleSheet" href="livewiki/<?php echo $atomjump_css_path ?>">
	
	<!-- Bootstrap HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="livewiki/<?php echo $html5shiv_path ?>"></script>
	  <script src="livewiki/<?php echo $respond_path ?>"></script>
	  <script src="livewiki/<?php echo $ie9_version ?>"></script>
	<![endif]-->
	
	<!-- Include your version of jQuery here. -->
	<script type="text/javascript" src="livewiki/<?php echo $jquery_js_path ?>"></script>
	
	<script>
		//Add your configuration here for AtomJump Feedback
		var ajFeedback = {
			"uniqueFeedbackId" : "<?php echo $unique_key . '-Home'; ?>",		//This can be anything globally unique to your company/page	
			"myMachineUser" : "192.104.113.117:1",			
			"server":  "<?php echo $server ?>",
			"cssFeedback": "livewiki/<?php echo $atomjump_css_path ?>",
			"cssBootstrap": "livewiki/<?php echo $bootstrap_css_path ?>",
			"domain" : "<?php echo $same_domain ?>"
		}
	</script>
	<script type="text/javascript" src="livewiki/<?php echo $atomjump_js_path ?>"></script>
<!-- AtomJump Feedback Ends -->
	</head>
	
	<body>
		<div>
			<div style="position: relative; width: 50%; margin-left: auto; margin-right: auto;">
				<p align="center" style="font-weight: bold;">AtomJump Messaging Appliance</p>
				<?php if($show_admin == true) { ?>
								<a href='/vendor/atomjump/loop-server/config/bootip.php'><p align="center">1. Update the Server Config</p></a>
				<?php } ?>
			
				<!-- Any link on the page can have the 'comment-open' class added and a blank 'href="javascript:"' -->
				<a class="comment-open" href="javascript:">
						<img style="width: 100%;" src="img/speech-bubble-start-1.png"><br/>
						<?php if($show_admin == true) { ?>
							<p align="center">2. Test your messaging</p>
						<?php } else { ?>
							<p align="center">1. Test your messaging</p>
						<?php } ?>
				</a>
				
				<?php if($show_admin == true) { ?>
					<p align="center">3. Set the admin user. <form style="text-align: center" action="/vendor/atomjump/loop-server/config/bootadmin.php"><input type="text" name="adminUserIP" placeholder="e.g. 192.115.117.49:2"><input type="submit" value="Submit" ><br/><small style="color: #BBB; text-align: center">In the <i>messaging box settings</i> enter an email and password,<br/>and then submit the  "Settings > More > Advanced > myMachineUser" value in the box above e.g.<br/>192.115.117.49:2</small></form></p>
					<a href='/livewiki/'><p align="center">4. Go to the Live Wiki</p></a>
				<?php } else { ?>
					<a href='/livewiki/'><p align="center">2. Go to the Live Wiki</p></a>
				<?php } ?>

			</div>
			 <br>
		</div>
		
		
		<!-- holds the popup comments. Can be anywhere between the <body> tags -->
		<div id="comment-holder"></div>
	
	</body>
	
</html>

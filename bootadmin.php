<?php
	//Run on starting a new server to get the latest ip of this server, and also set the adminUser.
	// Situate in loop-server/config/ folder
	include("db_connect.php");
	
	$old_admin = "92.27.10.17:2";
	if(isset($_REQUEST['adminUserIP'])) {
		$new_admin = trim($_REQUEST['adminUserIP']);			//e.g. Find this in Settings > More > Advanced. myMachineUser value e.g. 192.118.110.112:415
	} else {
		$new_admin = "92.27.10.17:2";
	}
	
	
	$already_set_admin_file = "/var/www/html/vendor/atomjump/loop-server/config/adminAlreadySet.json";
	
	//Check to see if we have already been set
	if(!file_exists($already_set_admin_file)) {
	
	
		$json = file_get_contents("/var/www/html/vendor/atomjump/loop-server/config/config.json");
	

		//Replace the new admin with the old default admin
		$json = str_replace($old_admin, $new_admin, $json);
						
		if(!file_put_contents("/var/www/html/vendor/atomjump/loop-server/config/config.json", $json)) {
			echo "Sorry, there is a permissions issue with writing a file. See bootadmin PHP script.";
			exit(0);
		}
	
		//Make sure we can't do this manually again
		if(file_put_contents($already_set_admin_file, "Yes, set already")) {
			echo "Successfully updated your administrative user! Please go back to the <a href=\"http://" . $_SERVER['HTTP_HOST'] . "\">homepage</a>.";
		} else {
			echo "Sorry, there is a permissions issue with writing a file. See bootadmin PHP script.";
		}
	
		
		//Get the system admin user's email address
		
		$old_email = "yourname@yourcompany.com";	//In our default plugins/help_is_coming/config.json
		$new_email = "";
		$warning = " Warning: there doesn't seem to be a user with that ID. You can configure this later by entering server<return>, then 'cd plugins/help_is_coming; sudo nano config.json' and changing the 'helperEmail' entry.";
		//Get the ID as the number after the ':'
		$ids = explode(":", $new_admin);
		if($ids[1]) {
			$sql = "SELECT * FROM tbl_user WHERE int_user_id = " . clean_data($ids[1]);
			$result = dbquery($sql)  or die("Unable to execute query $sql " . dberror());
			if($row = db_fetch_array($result)) {
				
				$new_email = $row['var_email'];
				
				
			} else {
				echo $warning;
			}
		} else {
			echo $warning;
		}
		
		//Now update the help_is_coming config
		$json = file_get_contents("/var/www/html/vendor/atomjump/loop-server/plugins/help_is_coming/config/config.json");
		$json = str_replace($old_email, $new_email, $json);
		
		if(!file_put_contents("/var/www/html/vendor/atomjump/loop-server/plugins/help_is_coming/config/config.json", $json)) {
			echo " Sorry, there is a permissions issue with writing the help_is_coming config file. See bootadmin PHP script.";
			exit(0);
		}
			
	
	} else {
	
		echo "Sorry, the admin user has already been set. Please login via the command-line to make any changes. Go back to the <a href=\"http://" . $_SERVER['HTTP_HOST'] . "\">homepage</a>.";
	}
?>

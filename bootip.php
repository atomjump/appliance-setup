<?php
	//Run on starting a new server to get the latest ip of this server. Situate in loop-server/config/ folder
	$new_site = "appliance-" . rand(0,10000000000) . "-";
	
	$json = file_get_contents("/var/www/html/vendor/atomjump/loop-server/config/configBOOT.json");
	$json = str_replace("[ajtestserver]", $_SERVER['HTTP_HOST'], $json);
	
	$regex = $new_site;
	$json = str_replace("yoursite_", $regex, $json);					
	file_put_contents("/var/www/html/vendor/atomjump/loop-server/config/config.json", $json);
	
	
	$php_code = file_get_contents("/var/www/html/livewiki/config.php");
	$php_code = str_replace("yoursite_", $new_site, $php_code);
	file_put_contents("/var/www/html/livewiki/config.php", $php_code);
	
	
	echo "Successfully updated your configuration! Please go back to the <a href=\"http://" . $_SERVER['HTTP_HOST'] . "\">homepage</a>.";

?>

<?php

		//Clears off all users and data of the system, so that the first user becomes the admin user
		
		//Connect to the database
		
		
		include_once("/var/www/html/vendor/atomjump/loop-server/config/db_connect.php");	
	
		echo "Warning: running this script will clear out any messages and uploaded files on the Appliance\n\n";
		echo "Please enter your database username (see loop-server-config-boot.json):";
		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		
		$db_username = trim($line);


		echo "Please enter your database password:";
		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		
		$db_password = trim($line);
		
		
		$db_host = $db_cnf['hosts'][$db_num];
					
					
				
														
		$db = dbconnect($db_host, $db_username, $db_password, null, $db_ssl, $db_port);
		
		if($db) {
					
			dbselect($db_name);
			db_set_charset('utf8mb4');
			db_misc();
			
			
		
			dbquery("DELETE FROM tbl_user");
			dbquery("DELETE FROM php_session");
			dbquery("DELETE FROM tbl_email");
			dbquery("DELETE FROM tbl_feed");
			dbquery("DELETE FROM tbl_layer");
			dbquery("DELETE FROM tbl_layer_subscription");
			dbquery("DELETE FROM tbl_search_suggest");
			dbquery("DELETE FROM tbl_ssshout");
			dbquery("DELETE FROM tbl_subdomain");
			
			dbquery("ALTER TABLE tbl_user AUTO_INCREMENT = 0");
			dbquery("ALTER TABLE tbl_ssshout AUTO_INCREMENT = 2867");
			
			echo "Finished clearing database\n";
		} else {
			echo "Error: sorry that was the wrong database user/password\n";
		
		}
		
		
		//Copy in the front-end files
		if(file_exists("/var/www/html/livewiki/css")) {
            	//Handle the livewiki interface
            	exec("sudo cp front-end/css/* /var/www/html/livewiki/css");
            	exec("sudo cp front-end/js/* /var/www/html/livewiki/js");
            	
            	
        }
        
        if(!is_dir("/var/www/html/img")) {
        		//Handle the homepage graphics
            	exec("sudo mkdir /var/www/html/img");
            	exec("sudo cp front-end/img/* /var/www/html/img");
        }
        
        
        //Copy the main homepage and other pages
        exec("sudo cp index.php /var/www/html/index.php");
        exec("sudo cp config.php /var/www/html/livewiki/config.php");
        exec("sudo chown www-data:www-data /var/www/html/livewiki/config.php");		//Give permissions to change
        exec("sudo chown www-data:www-data /var/www/html/vendor/atomjump/loop-server/config/");		//Give permissions to change
        exec("sudo cp loop-server-config-boot.json /var/www/html/vendor/atomjump/loop-server/config/configBOOT.json");
        exec("sudo cp messages.json /var/www/html/vendor/atomjump/loop-server/config/messages.json");
        exec("sudo cp bootip.php /var/www/html/vendor/atomjump/loop-server/config/bootip.php");
        exec("sudo cp bootadmin.php /var/www/html/vendor/atomjump/loop-server/config/bootadmin.php");
        exec("sudo cp htaccessBOOT /var/www/html/vendor/atomjump/loop-server/.htaccess");
        exec("sudo cp htaccessBOOT /var/www/html/vendor/atomjump/loop-server/config/.htaccess");			
        exec("sudo cp htaccessBOOT /var/www/html/.htaccess");
        exec("sudo cp words.json /var/www/html/livewiki/data/words.json");
        exec("sudo chmod 777 /var/www/html/livewiki/data/words.json");
        
        
        //Copy the plugin configs - note the plugins must be installed manually, first.
        if(!is_dir("/var/www/html/vendor/atomjump/loop-server/plugins/help_is_coming/")) {
        	echo "Warning: you need to install the following plugins manually from atomjump.org: help_is_coming, emoticons_large, notifications, emoticons_small, change_language, and then re-run this script.";
        } else {
       		exec("sudo cp plugins/help_is_coming/config.json /var/www/html/vendor/atomjump/loop-server/plugins/help_is_coming/config/config.json");
        	exec("sudo cp plugins/emoticons_large/config.json /var/www/html/vendor/atomjump/loop-server/plugins/emoticons_large/config/config.json");
        	exec("sudo cp plugins/notifications/config.json /var/www/html/vendor/atomjump/loop-server/plugins/notifications/config/config.json");
        	
        	exec("sudo chmod 777 /var/www/html/vendor/atomjump/loop-server/plugins/help_is_coming/config/config.json");		//Need to be able to update this one via a script (bootadmin.php)
        } 
        
        //Remove any testing photos
        exec("sudo rm /var/www/html/vendor/atomjump/loop-server/images/im/*.jpg");
        exec("sudo rm /var/www/html/vendor/atomjump/loop-server/images/im/*.mp4");
        exec("sudo rm /var/www/html/vendor/atomjump/loop-server/images/im/*.pdf");
        exec("sudo rm /var/www/html/vendor/atomjump/loop-server/images/im/*.mp3");
        
        //Make sure we cannot enter a new admin via the interface
        exec("sudo rm /var/www/html/vendor/atomjump/loop-server/config/adminAlreadySet.json");
        
        echo "\nFinished.\n";
        

?>

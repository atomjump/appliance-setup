# live-wiki-hosting
Internal scripts and tips for setting up live-wiki hosting

To update - use account 'localadmin' and 
```
git pull   #[note: no 'sudo' required]
```

Then to prepare a build for publication (warning: this will delete all messaging data on the virtual machine)
```
sudo php clear-owner.php
```


## For security

Copy and paste the apache2.conf file in this directory to your own apache2.conf file:

```
sudo cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf.GOOD   #backup first
sudo cp apache2.conf /etc/apache2/apache2.conf
sudo service apache2 restart
```

```
# AtomJump added: Remove access to files with extensions md|sql|txt|cnf|conf|json|log|sh|ini
<FilesMatch "\.(md|sql|txt|cnf|conf|json|log|sh|ini)$">
Order allow,deny
Deny from all
</FilesMatch>
```
